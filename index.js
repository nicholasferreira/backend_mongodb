require('dotenv').config()
require('./config/db')()

const consign = require('consign')
const app = require('express')()

consign()
    .include('./config/passport.js')
    .then('./config/middlewares.js')
    .then('./controllers')
    .then('./config/routes.js')
    .into(app)

app.listen(process.env.PORT, () =>
    console.log(`Servidor executando na porta ${process.env.PORT}`),
);