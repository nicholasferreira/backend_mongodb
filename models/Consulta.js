const mongoose = require('mongoose')

const ConsultaSchema = new mongoose.Schema({
  plataforma: {
    type: String,
    required: true,
    lowercase: true
  },
  device_info: {
    type: Object,
    required: true,
  },
  route: {
    type: String,
    required: true
  },
  dados: {
    type: Object,
    required: true
  },
  code: {
    type: String,
    required: true
  },
  msg: {
    type: String,
    required: true
  },
  sql: {
    type: String,
    required: true
  },
  data: {
    type: Date,
    required: true
  }
})

module.exports = mongoose.model('Consulta', ConsultaSchema)