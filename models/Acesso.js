const mongoose = require('mongoose')

const AcessoSchema = new mongoose.Schema({
  plataforma: {
    type: String,
    required: true,
    lowercase: true
  },
  device_info: {
    type: Object,
    required: true,
  },
  cliente: {
      type: Object,
      required: true,
  },
  data: {
    type: Date,
    required: true
  }
})

module.exports = mongoose.model('Acesso', AcessoSchema)