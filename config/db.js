const mongoose = require('mongoose');
require('require-dir')('../models')

module.exports = () => {
    const uri = process.env.MONGO_STR
    const database = process.env.MONGO_DATABASE

    mongoose.connect(`${uri}${database}?retryWrites=true&ssl=true&authSource=test`, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true
    })

    mongoose.connection.on('connected', () => {
        console.log('Conectado em ' + uri);
    });

    mongoose.connection.on('disconected', () => {
        console.log('Desconectado em ' + uri);
    });

    mongoose.connection.on('error', (erro) => {
        console.log('Erro na conexao em ' + erro);
    });

    process.on('SIGINT', () => {
        mongoose.connection.close(() => {
            console.log('Desconectado. Aplicacao encerrada');
            process.exit(0);
        });
    });
}