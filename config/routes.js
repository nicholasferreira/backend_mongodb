module.exports = app => {

    app.route('/')
    .get((req, res, next) => {
        res.status(200).send({ api: process.env.NAME, version: process.env.VERSION })
    })

    app.route('/consultas')
        .all(app.config.passport.authenticate())
        .post(app.controllers.logConsulta.novo)

    app.route('/acessos')
        .all(app.config.passport.authenticate())
        .post(app.controllers.logAcesso.novo)
}